# -*- coding: utf-8 -*-

import data
import logging
import binary
from galaxy.datatypes import metadata
from galaxy.datatypes.metadata import MetadataElement
from galaxy.datatypes.sniff import *
import commands

import BALL

log = logging.getLogger(__name__)

class NMRFeatureFile( data.Text ):
  file_ext = "nmrfeaturefile"

  def get_mime(self):
    return 'text/plain'

class NMRModel( binary.Binary ):
  file_ext = "nmrmodel"

  def get_mime(self):
    return 'application/octet-stream'

	
