# -*- coding: utf-8 -*-

import data
import logging
from galaxy.datatypes import metadata
from galaxy.datatypes.metadata import MetadataElement
from galaxy.datatypes.sniff import *
import commands

import BALL

log = logging.getLogger(__name__)

class GenericMolFile( data.Text ):
  MetadataElement(name="comments", default="", desc="Comments", readonly=True, optional=True, visible=True, no_value="")
  def init_meta( self, dataset, copy_from=None ):
     data.Text.init_meta( self, dataset, copy_from=copy_from )
  def set_meta( self, dataset, overwrite = True, skip = None, max_data_lines = 100000, **kwd):
    dataset.metadata.comments = ""
    dataset.info = commands.getoutput("grep BOA_Constructor_penalty " + dataset.file_name).replace("=", ":")
  file_ext = "mol2/sdf/drf/pdb/mol/hin/ac/xyz"
  no_mols = 0
  def check_filetype( self,filename ):
    # try all formats we support one by one
		print "checking filetype now"
		try:
			print "trying mol2"
			f = BALL.MOL2File(filename, BALL.File.MODE_IN)
			S = BALL.System()
			if (f.read(S)):
				print "read mol2"
				print S.countAtoms()
				if (S.countAtoms() > 0):
					self.file_ext = "mol2"
					self.no_mols = S.countMolecules()
					return True
		except:
			# it is not a MOL2File, let's try the next one
			None

		try:
			print "testing sdf"
			f = BALL.SDFile(filename, BALL.File.MODE_IN)
			S = BALL.System()
			if (f.read(S)):
				if (S.countAtoms() > 0):
					self.file_ext = "sdf"
					self.no_mols = S.countMolecules()
					return True
		except:
			# it is not an SDFile, let's try the next one
			None
		
		try:
			print "testing drf"
			tmp = commands.getstatusoutput("grep -c \"ligand id\" "+filename)
			if (tmp[0] == 0) & (tmp[1] > 0):
				self.no_mols = tmp[1]
				self.file_ext="drf"
				return True
		except:
			# it is not an DRFile, let's try the next one
			None
		
		try:
			print "testing pdb"
			f = BALL.PDBFile(filename, BALL.File.MODE_IN)
			S = BALL.System()
			if (f.read(S)):
				if (S.countAtoms() > 0):
					self.file_ext = "pdb"
					self.no_mols = S.countMolecules()
					return True
		except:
			# it is not a PDBFile, let's try the next one
			None
		
		try:
			print "testing mol"
			f = BALL.MOLFile(filename, BALL.File.MODE_IN)
			S = BALL.System()
			if (f.read(S)):
				if (S.countAtoms() > 0):
					self.file_ext = "mol"
					self.no_mols = S.countMolecules()
					return True
		except:
			# it is not a MOLFile, let's try the next one
			None
		
		try:
			print "testing hin"
			f = BALL.HINFile(filename, BALL.File.MODE_IN)
			S = BALL.System()
			if (f.read(S)):
				if (S.countAtoms() > 0):
					self.file_ext = "hin"
					self.no_mols = S.countMolecules()
					return True
		except:
			# it is not a HINFile, let's try the next one
			None
		
		try:
			print "testing ac"
			f = BALL.antechamberFile(filename, BALL.File.MODE_IN)
			S = BALL.System()
			if (f.read(S)):
				if (S.countAtoms() > 0):
					self.file_ext = "ac"
					self.no_mols = S.countMolecules()
					return True
		except:
			# it is not a antechamberFile, let's try the next one
			None

		try:
			print "testing xyz"
			f = BALL.XYZFile(filename)
			S = BALL.System()
			if (f.read(S)):
				if (S.countAtoms() > 0):
					self.file_ext = "xyz"
					self.no_mols = S.countMolecules()
					return True
		except:
			# it is not an XYZFile, so we don't know what it is
			None

		return False

  def set_peek( self, dataset, is_multi_byte=False ):
    if not dataset.dataset.purged:
      if(self.check_filetype(dataset.file_name)) :
        if (self.no_mols == '1'):
          dataset.blurb = "1 molecule"
        else:
          dataset.blurb = "%s molecules"%self.no_mols
      dataset.peek = data.get_file_peek( dataset.file_name, is_multi_byte=is_multi_byte )
    else:
      dataset.peek = 'file does not exist'
      dataset.blurb = 'file purged from disk'

  def get_mime(self):
    return 'text/plain'


class GenericMultiMolFile( GenericMolFile ):
  def set_peek( self, dataset, is_multi_byte=False ):
    if not dataset.dataset.purged:
      self.sniff(dataset.file_name)
      if (self.no_mols == '1'):
        dataset.blurb = "1 molecule"
      else:
        dataset.blurb = "%s molecules"%self.no_mols
      dataset.peek = data.get_file_peek( dataset.file_name, is_multi_byte=is_multi_byte )
    else:
      dataset.peek = 'file does not exist'
      dataset.blurb = 'file purged from disk'
 
class MOL2( GenericMultiMolFile ):
  file_ext = "mol2"
  def sniff( self, filename ):
		self.check_filetype(filename)
		if self.file_ext == "mol2":
			return True
		else:
			return False

class SDF( GenericMultiMolFile ):
  file_ext = "sdf"
  def sniff( self, filename ):
		self.check_filetype(filename)
		if self.file_ext == "sdf":
			return True
		else:
			return False

class DRF( GenericMultiMolFile ):
  file_ext = "drf"
  def sniff( self, filename ):
		self.check_filetype(filename)
		if self.file_ext == "drf":
			return True
		else:
			return False

class PDB( GenericMolFile ):
  file_ext = "pdb"
  def sniff( self, filename ):
		self.check_filetype(filename)
		if self.file_ext == "pdb":
			return True
		else:
			return False

  def set_peek( self, dataset, is_multi_byte=False ):
  #def set_peek( self, dataset, line_count=None, is_multi_byte=False ):
    if not dataset.dataset.purged:
      res = commands.getstatusoutput("lib/galaxy/datatypes/countResidues.sh "+dataset.file_name)
      dataset.peek = res[1]
      self.sniff(dataset.file_name)
      if (self.no_mols == '1'):
        dataset.blurb = "1 protein structure"
      else:
        dataset.blurb = "%s protein structures"%self.no_mols
    else:
      dataset.peek = 'file does not exist'
      dataset.blurb = 'file purged from disk'

class MOL( GenericMultiMolFile ):
  file_ext = "mol"
  def sniff( self, filename ):
		self.check_filetype(filename)
		if self.file_ext == "mol":
			return True
		else:
			return False

class HIN( GenericMultiMolFile ):
  file_ext = "hin"
  def sniff( self, filename ):
		self.check_filetype(filename)
		if self.file_ext == "hin":
			return True
		else:
			return False

class Antechamber( GenericMultiMolFile ):
  file_ext = "ac"
  def sniff( self, filename ):
		self.check_filetype(filename)
		if self.file_ext == "ac":
			return True
		else:
			return False

class XYZ( GenericMultiMolFile ):
  file_ext = "xyz"
  def sniff( self, filename ):
		self.check_filetype(filename)
		if self.file_ext == "xyz":
			return True
		else:
			return False

class grd ( data.Text ) :
  file_ext = "grd"
  def set_peek( self, dataset, is_multi_byte=False ):
    if not dataset.dataset.purged:
      #dataset.peek = ""
      dataset.blurb = "score-grids for docking"
    else:
      dataset.peek = 'file does not exist'
      dataset.blurb = 'file purged from disk'

class grdtgz ( data.Text ) :
  file_ext = "grd.tgz"
  def set_peek( self, dataset, is_multi_byte=False ):
    if not dataset.dataset.purged:
      #dataset.peek = ""
      dataset.blurb = "compressed score-grids for docking"
    else:
      dataset.peek = 'file does not exist'
      dataset.blurb = 'file purged from disk'

